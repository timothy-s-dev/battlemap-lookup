import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BattlemapModule } from './battlemap/battlemap.module';

@Module({
  imports: [BattlemapModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
