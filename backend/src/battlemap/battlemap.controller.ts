import { Controller, Get, Param } from '@nestjs/common';
import { BattlemapService } from './battlemap.service';

@Controller('battlemap')
export class BattlemapController {
  constructor(private battlemapService: BattlemapService) { }

  @Get('/:id')
  async getBattlemap(@Param('id') id: string) {
    return {
      id,
      name: 'test',
    };
  }
}
