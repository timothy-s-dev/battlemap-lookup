import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { BattlemapService } from './battlemap.service';
import { BattlemapController } from './battlemap.controller';
import { AuthenticationMiddleware } from 'src/common/authentication.middleware';

@Module({
  providers: [BattlemapService],
  controllers: [BattlemapController]
})
export class BattlemapModule {
  configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
    consumer.apply(AuthenticationMiddleware).forRoutes(
      { method: RequestMethod.GET, path: '/battlemap/:id' },
    );
  }
}
