import { Test, TestingModule } from '@nestjs/testing';
import { BattlemapController } from './battlemap.controller';

describe('Battlemap Controller', () => {
  let controller: BattlemapController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BattlemapController],
    }).compile();

    controller = module.get<BattlemapController>(BattlemapController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
