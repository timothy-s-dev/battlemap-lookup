import { Test, TestingModule } from '@nestjs/testing';
import { BattlemapService } from './battlemap.service';

describe('BattlemapService', () => {
  let service: BattlemapService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BattlemapService],
    }).compile();

    service = module.get<BattlemapService>(BattlemapService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
