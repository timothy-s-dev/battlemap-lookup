import React, { useState } from 'react';
import { useAuth0 } from '../contexts/auth0-context';
import { useHistory } from 'react-router-dom';

const CreateBattlemapPage = () => {
  const { isLoading, isAuthenticated, user, loginWithRedirect, getIdTokenClaims } = useAuth0();
  let history = useHistory();

  const [title, setTitle] = useState('loading...');

  if (isLoading) {
    return <></>; // TODO: Render spinner?
  }

  if (!isLoading && !isAuthenticated) {
    loginWithRedirect();
    history.push('/');
    return <></>;
  }
  getIdTokenClaims().then((accessToken: any) => {
    fetch(`${process.env.REACT_APP_SERVER_BASE_URL}/battlemap/id-goes-here`, {
      method: "get",
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json",
        "authorization": `Bearer ${accessToken.__raw}`
      })
    }).then(async (response) => {
      setTitle(await response.text());
    });
  });

  return <>
    <div>{user.name}</div>
    <div>{title}</div>
  </>;
}

export default CreateBattlemapPage;