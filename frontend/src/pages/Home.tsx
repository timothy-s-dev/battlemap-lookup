import logo from '../logo.svg';
import React from 'react';
import { useAuth0 } from '../contexts/auth0-context';

const HomePage = () => {
  const { user } = useAuth0();

  return <header className="App-header">
    <img src={logo} className="App-logo" alt="logo" />
    <p>
      Edit <code>src/App.tsx</code> and save to reload.
    </p>
    {user && user.name &&
      <p>Signed in as {user.name}.</p>
    }
    <a
      className="App-link"
      href="https://reactjs.org"
      target="_blank"
      rel="noopener noreferrer"
    >
      Learn React
    </a>
  </header>;
}

export default HomePage;