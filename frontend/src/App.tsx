import React from 'react';
import './App.css';
import HomePage from './pages/Home';
import { Switch, Route } from 'react-router-dom';
import CreateBattlemapPage from './pages/CreateBattlemap';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path={"/"} exact={true} component={HomePage} />
        <Route path={"/new"} component={CreateBattlemapPage} />
      </Switch>
    </div>
  );
}

export default App;
