FROM gitpod/workspace-full

USER gitpod

WORKDIR /home/gitpod
ADD https://s3.us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.tar.gz dynamodb/
WORKDIR /home/gitpod/dynamodb
RUN sudo chmod +rx dynamodb_local_latest.tar.gz
RUN sudo tar -xzf dynamodb_local_latest.tar.gz && sudo rm dynamodb_local_latest.tar.gz
RUN sudo chown gitpod /home/gitpod/dynamodb -R && sudo chmod 775 -R /home/gitpod/dynamodb

WORKDIR /home/gitpod
ADD https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip aws/
WORKDIR /home/gitpod/aws
RUN sudo unzip awscli-exe-linux-x86_64.zip
RUN sudo ./aws/install

WORKDIR /home/gitpod
ADD https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.9.0-linux-x86_64.tar.gz elasticsearch/
WORKDIR /home/gitpod/elasticsearch
RUN sudo chmod +rx elasticsearch-7.9.0-linux-x86_64.tar.gz
RUN sudo tar -xzf elasticsearch-7.9.0-linux-x86_64.tar.gz
RUN sudo chown gitpod /home/gitpod/elasticsearch -R && sudo chmod 775 -R /home/gitpod/elasticsearch